package mflix.api.daos;

import com.mongodb.ErrorCategory;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import mflix.api.models.Session;
import mflix.api.models.User;

import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@Configuration
public class UserDao extends AbstractMFlixDao {

    private final MongoCollection<User> usersCollection;
    //TODO> Ticket: User Management - do the necessary changes so that the sessions collection
    //returns a Session object
    private final MongoCollection<Session> sessionsCollection;

    private final Logger log;

    @Autowired
    public UserDao(
            MongoClient mongoClient, @Value("${spring.mongodb.database}") String databaseName) {
        super(mongoClient, databaseName);
        CodecRegistry pojoCodecRegistry =
                fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        usersCollection = db.getCollection("users", User.class).withCodecRegistry(pojoCodecRegistry);
        log = LoggerFactory.getLogger(this.getClass());
        //TODO> Ticket: User Management - implement the necessary changes so that the sessions
        // collection returns a Session objects instead of Document objects.
        sessionsCollection = db.getCollection("sessions", Session.class)
        		.withCodecRegistry(pojoCodecRegistry)
        		.withWriteConcern(WriteConcern.MAJORITY);// you might want to use a more durable write concern here
    }

    /**
     * Inserts the `user` object in the `users` collection.
     *
     * @param user - User object to be added
     * @return True if successful, throw IncorrectDaoOperation otherwise
     */
    public boolean addUser(User user) {
        //TODO > Ticket: Durable Writes -  you might want to use a more durable write concern here!
        try {
			usersCollection.withWriteConcern(WriteConcern.MAJORITY).insertOne(user);
			
		} catch (MongoException e) {
		      if (ErrorCategory.fromErrorCode( e.getCode() ) == ErrorCategory.DUPLICATE_KEY) {
		        throw new IncorrectDaoOperation("The User is already in the database.");
		      }
		}
        //TODO > Ticket: Handling Errors - make sure to only add new users
        // and not users that already exist.
        return true;
    }

    /**
     * Creates session using userId and jwt token.
     *
     * @param userId - user string identifier
     * @param jwt    - jwt string token
     * @return true if successful
     */
    public boolean createUserSession(String userId, String jwt) {
        //TODO> Ticket: User Management - implement the method that allows session information to be
        // stored in it's designated collection.
    	Session session = new Session();
    	session.setUserId(userId);
    	session.setJwt(jwt);
    	
    	Bson queryFilter = Filters.eq("user_id", userId);
    	Bson docUp=new Document("$set", session);
	    UpdateOptions options = new UpdateOptions();
	    options.upsert(true);

	    try {
			// and adding those options to the update method.
			sessionsCollection.updateOne(queryFilter, docUp, options);
			
			log.info(jwt);
			
			return true;
		} catch (IncorrectDaoOperation e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return false;
        //TODO > Ticket: Handling Errors - implement a safeguard against
        // creating a session with the same jwt token.
    }

    /**
     * Returns the User object matching the an email string value.
     *
     * @param email - email string to be matched.
     * @return User object or null.
     */
    public User getUser(String email) {
        User user = null;
        //TODO> Ticket: User Management - implement the query that returns the first User object.
//        Document queryFilter = new Document("email", email);
        Bson queryFilter = Filters.eq("email", email);
        user = usersCollection
        			.find(queryFilter)
        			.first();
//        			.limit(1)
//        			.iterator()
//        			.next();
        
//        Bson queryFilter1 = Filters.eq("email", email);
//        Bson matchStage = Aggregates.match(queryFilter1);
//        Bson limitStage = Aggregates.limit(1);
//        
//        List<Bson> limitFirstPipeline = new ArrayList<>();
//        limitFirstPipeline.add(matchStage);
//        limitFirstPipeline.add(limitStage);
//        
//        user=usersCollection.aggregate(limitFirstPipeline).first();

        
        return user;
    }

    /**
     * Given the userId, returns a Session object.
     *
     * @param userId - user string identifier.
     * @return Session object or null.
     */
    public Session getUserSession(String userId) {
        //TODO> Ticket: User Management - implement the method that returns Sessions for a given
        // userId
    	
    	Session session=null;

//    	Document queryFilter = new Document("user_id", userId);
    	Bson queryFilter = Filters.eq("user_id", userId);
        session = sessionsCollection
        			.find(queryFilter)
        			.first();

    	
    	
        return session;
    }

    public boolean deleteUserSessions(String userId) {
        //TODO> Ticket: User Management - implement the delete user sessions method
    	Bson queryFilter = Filters.eq("user_id", userId);

        DeleteResult dResult = sessionsCollection.deleteMany(queryFilter);
    	log.info("*****************************************"+dResult.getDeletedCount());
        return true;
    }

    /**
     * Removes the user document that match the provided email.
     *
     * @param email - of the user to be deleted.
     * @return true if user successfully removed
     */
    public boolean deleteUser(String email) {
        // remove user sessions
    	Bson queryFilterU = Filters.eq("email", email);
        //TODO> Ticket: User Management - implement the delete user method    	
    	Bson queryFilterS = Filters.eq("user_id", email);
    	
        try {
			DeleteResult dResultS = sessionsCollection.deleteOne(queryFilterS);
			DeleteResult dResultU = usersCollection.deleteOne(queryFilterU);

			log.info("*****************************************"+dResultS.getDeletedCount());
			log.info("*****************************************"+dResultU.getDeletedCount());
			return true;
		} catch (IncorrectDaoOperation e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        return false;
    	
        //TODO > Ticket: Handling Errors - make this method more robust by
        // handling potential exceptions.
//        return false;
    }

    /**
     * Updates the preferences of an user identified by `email` parameter.
     *
     * @param email           - user to be updated email
     * @param userPreferences - set of preferences that should be stored and replace the existing
     *                        ones. Cannot be set to null value
     * @return User object that just been updated.
     */
    public boolean updateUserPreferences(String email, Map<String, ?> userPreferences) {
        //TODO> Ticket: User Preferences - implement the method that allows for user preferences to
        // be updated.
    	Bson queryFilter = Filters.eq("email", email);
    	

			try {
				usersCollection.updateOne(queryFilter,set("preferences",
						Optional.ofNullable(userPreferences).orElseThrow( () -> 
				        new IncorrectDaoOperation("user preferences cannot be null") ) ));
		        return true;
			} catch (IncorrectDaoOperation e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

    	  
    	  
        //TODO > Ticket: Handling Errors - make this method more robust by
        // handling potential exceptions when updating an entry.
        return false;
    }
}
